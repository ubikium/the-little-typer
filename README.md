# Exercises from *The Little Typer*

This repository also includes a keybinding file to input fancy unicode symbols in Dr. Racket.
From the menu, `Edit` - `Keybindings` - `Add User-defined Keybindings...` and select this file.
`d:r` is `Cmd+r`, which inputs `→`, and so on.
`d:s:s` is `Cmd+Shift+s`, which input `Σ`. (`Cmd+s` is save definition.)

For an extensive setup guide, tips, and review, see [my blog post](https://ubikium.gitlab.io/portfolio/tips-and-reviews-of-the-little-typer.html).
