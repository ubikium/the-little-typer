#lang pie
(claim +
  (→ Nat Nat
    Nat))
(claim step-+
  (→ Nat
    Nat))
(define step-+
  (λ (+n-1)
    (add1 +n-1)))
(define +
  (λ (i j)
    (iter-Nat i
      j
      step-+)))

(claim =consequence
  (Π ((n Nat)
      (j Nat))
    U))
(define =consequence
  (λ (n j)
    (which-Nat n
      (which-Nat j
        Trivial
        (λ (j-1)
          Absurd))
      (λ (n-1)
        (which-Nat j
          Absurd
          (λ (j-1)
            (= Nat n-1 j-1)))))))

(claim =consequence-same
  (Π ((n Nat))
    (=consequence n n)))
(define =consequence-same
  (λ (n)
    (ind-Nat n
      (λ (k) (=consequence k k))
      sole
      (λ (n-1 almost)
        (same n-1)))))

(claim use-Nat=
  (Π ((n Nat)
      (j Nat))
    (→ (= Nat n j)
      (=consequence n j))))
(define use-Nat=
  (λ (n j n=j)
    (replace n=j
      (λ (k) (=consequence n k))
      (=consequence-same n))))

(claim zero-not-add1
  (Π ((n Nat))
    (→ (= Nat zero (add1 n))
      Absurd)))
(define zero-not-add1
  (λ (n)
    (use-Nat= zero (add1 n))))

(claim Dec
  (→ U
    U))
(define Dec
  (λ (X)
    (Either X
      (→ X Absurd))))

(claim zero?
  (Π ((j Nat))
    (Dec
     (= Nat zero j))))
(define zero?
  (λ (n)
    (ind-Nat n
      (λ (k)
        (Dec
          (= Nat zero k)))
      (left (same zero))
      (λ (k-1 almost)
        (right (zero-not-add1 k-1))))))

(claim nat=?
  (Π ((n Nat)
      (j Nat))
    (Dec
     (= Nat n j))))
(claim mot-nat=?
  (→ Nat
    U))
(define mot-nat=?
  (λ (k)
    (Π ((j Nat))
      (Dec
       (= Nat k j)))))
(define nat=?
  (λ (n j)
    ((ind-Nat n
       mot-nat=?
       zero?
       (λ (n-1 ih)
         (λ (j)
           (ind-Nat j
             (λ (k)
               (Dec
                 (= Nat (add1 n-1) k)))
             (right (use-Nat= (add1 n-1) zero))
             (λ (j-1 almost)
               (ind-Either (ih j-1)
                 (λ (either)
                   (Dec
                     (= Nat (add1 n-1) (add1 j-1))))
                 (λ (heq)
                   (left
                     (cong heq (+ 1))))
                 (λ (hneq)
                   (right
                     (λ (hcontra)
                       (hneq (use-Nat= (add1 n-1) (add1 j-1) hcontra)))))
                 ))))))
          
      j)))