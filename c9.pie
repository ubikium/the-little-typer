#lang pie

(claim +
  (→ Nat Nat
    Nat))
(claim step-+
  (→ Nat
    Nat))
(define step-+
  (λ (+n-1)
    (add1 +n-1)))
(define +
  (λ (i j)
    (iter-Nat i
      j
      step-+)))

(claim incr
  (→ Nat
    Nat))
(define incr
  (λ (n)
    (iter-Nat n
      1
      (+ 1))))

(claim step-incr=add1
  (Π ((n-1 Nat))
    (→ (= Nat
         (incr n-1)
         (add1 n-1))
      (= Nat
        (add1 (incr n-1))
        (add1 (add1 n-1))))))

(claim mot-step-incr=add1
  (→ Nat Nat
    U))
(define mot-step-incr=add1
  (λ (n-1 k)
    (= Nat
      (add1 (incr n-1))
      (add1 k))))

(define step-incr=add1
  (λ (n-1 ih)
    (replace ih
      (mot-step-incr=add1 n-1)
      (same (add1 (incr n-1))))))

(claim double
  (→ Nat
    Nat))
(define double
  (λ (n)
    (iter-Nat n
      0
      (+ 2))))

(claim twice
  (→ Nat
    Nat))
(define twice
  (λ (n)
    (+ n n)))

(claim twice=double
  (Π ((n Nat))
    (= Nat (twice n) (double n))))
(claim add1+=+add1
  (Π ((n Nat)
      (j Nat))
    (= Nat
       (add1 (+ n j))
       (+ n (add1 j)))))
(define add1+=+add1
  (λ (n j)
    (ind-Nat n
      (λ (k)
        (= Nat
          (add1 (+ k j))
          (+ k (add1 j))))
      (same (add1 j))
      (λ (n-1 ih)
        (cong ih (+ 1))))))
(define twice=double
  (λ (n)
    (ind-Nat n
      (λ (k)
        (= Nat (twice k) (double k)))
      (same zero)
      (λ (n-1 ih)
        (replace
          (add1+=+add1 n-1 n-1)
          (λ (k)
            (= Nat
              (add1 k)
              (add1 (add1 (double n-1)))))
          (cong ih (+ 2)))))))

(claim twice-Vec
  (Π ((E U)
      (l Nat))
    (→ (Vec E l)
      (Vec E (twice l)))))
(claim double-Vec
  (Π ((E U)
      (l Nat))
    (→ (Vec E l)
      (Vec E (double l)))))
(define double-Vec
  (λ (E l)
    (ind-Nat l
      (λ (k)
        (→ (Vec E k)
           (Vec E (double k))))
      (λ (es) vecnil)
      (λ (l-1 almost es)
        (vec:: (head es) (vec:: (head es) (almost (tail es))))))))

(claim mushroom
  (Vec Atom 1))
(define mushroom
  (vec:: 'shitake vecnil))
(claim mushrooms
  (Vec Atom 2))
(define mushrooms
  (vec:: 'shitake (vec:: 'truffle vecnil)))

(define twice-Vec
  (λ (E l es)
    (replace
      (symm (twice=double l))
      (λ (k)
        (Vec E k))
      (double-Vec E l es))))