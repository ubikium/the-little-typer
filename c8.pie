#lang pie
(claim +
  (→ Nat Nat
    Nat))
(claim step-+
  (→ Nat
    Nat))
(define step-+
  (λ (+n-1)
    (add1 +n-1)))
(define +
  (λ (i j)
    (iter-Nat i
      j
      step-+)))

(claim incr
  (→ Nat
    Nat))
(define incr
  (λ (n)
    (iter-Nat n
      1
      (+ 1))))

(claim +1=add1
  (Π ((n Nat))
    (= Nat (+ 1 n) (add1 n))))
(define +1=add1
  (λ (n)
    (same (add1 n))))

(claim incr=add1
  (Π ((n Nat))
    (= Nat (incr n) (add1 n))))
(claim base-incr=add1
  (= Nat (incr zero) (add1 zero)))
(define base-incr=add1
  (same (add1 zero)))
(claim mot-incr=add1
  (→ Nat
    U))
(define mot-incr=add1
  (λ (l)
    (= Nat (incr l) (add1 l))))
(claim step-incr=add1
  (Π ((n-1 Nat))
    (→ (= Nat (incr n-1) (add1 n-1))
      (= Nat (add1 (incr n-1)) (add1 (add1 n-1))))))
(define step-incr=add1
  (λ (n-1 ih)
    (cong ih (+ 1))))
(define incr=add1
  (λ (n)
    (ind-Nat n
      mot-incr=add1
      base-incr=add1
      step-incr=add1)))
